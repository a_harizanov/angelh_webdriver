<?php
/**
 * @file
 * Contains \Angelh\WebDriver\BrowserConfig.
 */

namespace Angelh\WebDriver;

use Symfony\Component\Yaml\Parser;

/**
 * Singleton holding a list of browser configurations for different Selenium
 * server environments.
 *
 * @package Angelh\WebDriver
 */
class BrowserConfig
{

    static protected $config = array();

    /**
     * Read browser configuration from a YAML file.
     *
     * When calling this method multiple times, the newly read configuration is
     * merged with the old one,
     * in such a way that:
     * - new environments are added
     * - existing configuration for a specific environment are replaced
     *
     * @param string $file
     *   Path to the YAML file.
     */
    public static function read($file)
    {
        $yaml = new Parser();

        $value = $yaml->parse(file_get_contents($file));

        self::$config = $value + self::$config;
    }

    /**
     * Get the browser configuration for a specific environment.
     *
     * @param string $environment
     *   The environment to run the tests on. For example 'saucelabs'.
     *
     * @return array
     *   A list of browser configurations.
     */
    public static function getForEnvironment($environment)
    {
        if (isset(self::$config[$environment])) {
            return self::$config[$environment];
        }

        // @todo Throw an exception instead if environment unknown.
        return array();
    }
}
