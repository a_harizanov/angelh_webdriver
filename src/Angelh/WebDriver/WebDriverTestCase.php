<?php
/**
 * Base class for the Angelh Selenium test
 */
namespace Angelh\WebDriver;

use Jeremeamia\SuperClosure\SerializableClosure;

class WebDriverTestCase extends \PHPUnit_Extensions_Selenium2TestCase
{
    /**
     * The base URL used for HTTP requests to the website under test.
     *
     * @var string
     */
    public $base_url;

    public static $browsers = array();

    private static $firstTestStartedTime;

    /**
     * Toggle to maximize the browser window or not before running the actual
     * test.
     *
     * @var bool
     */
    protected $maximizeWindow = false;

    /**
     * Milliseconds to wait implicitly. 0 means use the Selenium server default.
     *
     * @var int
     */
    protected $implicitWait = 0;

    // Maximum time we to wait when searching for an element.
    protected $timeout = "30000";

    // Parameters to initialize a browser session with.
    protected $browserParams;

    public static function suite($className)
    {
        self::browserSetup();
        return \PHPUnit_Extensions_SeleniumTestSuite::fromTestCaseClass($className);
    }


    public static function browserSetup()
    {
        $environment = self::getEnvironment();
        self::$browsers = BrowserConfig::getForEnvironment($environment);
    }

    /**
     * Get the identifier of the current Selenium environment, eg. "saucelabs".
     *
     * The current environment is defined by the environment variable
     * 'selenium_location'. You can either pass this environment variable on to
     * the phpunit command when running it, or set it in the phpunit.xml
     * configuration file.
     *
     * If the environment variable is not set, 'default' will be used.
     *
     * @return string
     *   Identifier of the current Selenium environment.
     */
    protected static function getEnvironment()
    {
        $environment = getenv('selenium_location');

        if (!$environment) {
            $environment = 'default';
        }

        return $environment;
    }

    public function setUp()
    {
        // Set base url if provided
        $base_url = getenv('base_url');
        if ($base_url) {
            $this->base_url = $base_url;
            $this->setBrowserUrl($base_url);
        }

        parent::setUp();

        if ($this->maximizeWindow) {
            $this->prepareSession()->currentWindow()->maximize();
        }

        if ($this->implicitWait) {
            $this->timeouts()->implicitWait($this->implicitWait);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareSession()
    {
        $session = null;
        // Fail the test rather than marking it as skipped when the connection to
        // the server is lost.
        try {
            $session = parent::prepareSession();
        } catch (\PHPUnit_Framework_SkippedTestError $e) {
            $host = $this->getHost();
            $port = $this->getPort();
            $this->fail("The Selenium Server is not active on host $host at port $port.");
        }
        return $session;
    }

    /**
     * Get the current path, relative to the base url.
     */
    public function path()
    {
        $url = $this->url();

        if (0 === strpos($url, $this->base_url)) {
            $path = substr($url, strlen($this->base_url));
        } else {
            $path = $url;
        }

        return $path;
    }

    /**
     * Returns the time the first test started.
     * @return int
     *   Timestamp.
     */
    public function firstTestStartedTime()
    {
        if (!isset(self::$firstTestStartedTime)) {
            self::$firstTestStartedTime = time();
        }

        return self::$firstTestStartedTime;
    }

    /**
     * Wait until element is present.
     *
     * @param $xpath string a xpath query string
     */
    public function waitUntilElementIsPresent($xpath)
    {
        $testcase = $this;
        $callable = new SerializableClosure(
            function () use ($testcase, $xpath) {
                try {
                    $element = $testcase->element($testcase->using('xpath')->value($xpath));
                    if (!empty($element)) {
                        return true;
                    }
                } catch (\Exception $e) {
                    return false;
                }
            }
        );
        $this->waitUntil($callable, $this->timeout);
    }

    /**
     * Wait until text is present.
     * @param $text string that needs to be present
     */
    public function waitUntilTextIsPresent($text)
    {
        $testcase = $this;
        $callable = new SerializableClosure(
            function () use ($testcase, $text) {
                try {
                    if ($testcase->isTextPresent($text)) {
                        return true;
                    }
                } catch (\Exception $e) {
                    return false;
                }
            }
        );
        $this->waitUntil($callable, $this->timeout);
    }

    /**
     * @param string $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @return string
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Whether to enable Paddle apps via cron or not.
     *
     * False by default.
     *
     * @return boolean
     */
    public function getEnableAppsViaCron()
    {
        return (array_key_exists('enableAppsViaCron', $this->browserParams)
            && $this->browserParams['enableAppsViaCron'] == true);
    }

    /**
     * Wait until element is no longer present.
     * @param $xpath string a xpath query string
     */
    public function waitUntilElementIsNoLongerPresent($xpath)
    {
        $testcase = $this;
        $callable = new SerializableClosure(
            function () use ($testcase, $xpath) {
                try {
                    $element = $testcase->elements($testcase->using('xpath')->value($xpath));
                    if (empty($element)) {
                        return true;
                    }
                } catch (\Exception $e) {
                    return false;
                }
            }
        );
        $this->waitUntil($callable, $this->timeout);
    }


    /**
     * Wait until element is not displayed.
     * @param $xpath string a xpath query string
     */
    public function waitUntilElementIsNotDisplayed($xpath)
    {
        $testcase = $this;
        $callable = new SerializableClosure(
            function () use ($testcase, $xpath) {
                try {
                    $element = $testcase->element($testcase->using('xpath')->value($xpath));
                    if (!$element->displayed()) {
                        return true;
                    }
                } catch (\Exception $e) {
                    return false;
                }
            }
        );
        $this->waitUntil($callable, $this->timeout);
    }

    /**
     * {@inheritdoc}
     */
    public function setupSpecificBrowser($params)
    {
        // Keep the browser params, so we can use them in a template
        // when running tests in isolated processes. See prepareTemplate().
        $this->browserParams = $params;
        parent::setupSpecificBrowser($params);

        if (isset($params['maximizeWindow'])) {
            $this->maximizeWindow = $params['maximizeWindow'];
        }

        if (isset($params['implicitWait'])) {
            $this->implicitWait = $params['implicitWait'];
        }
    }


    /**
     * Executes a callback in a new window.
     *
     * Opens a new window, executes the callback, closes the window and puts
     * back the focus in the initial window.
     *
     * @param string $url
     *   The url which should be opened in the new window.
     * @param callable|null $callback
     *   The callable function to call after the new window has been opened.
     */
    public function openInNewWindow($url = '', $callback = null)
    {
        $url = $url ?: $this->base_url;
        $main_window_handle = $this->windowHandle();

        // @see https://github.com/sebastianbergmann/phpunit-selenium/issues/160
        $this->execute(
            array(
              'script' => 'window.open("' . $url . '");',
              'args' => array(),
            )
        );

        $handles = $this->windowHandles();
        $new_window = array_pop($handles);
        $this->window($new_window);

        if ($callback) {
            $callback($this);
        }

        $this->closeWindow();

        $this->window($main_window_handle);
    }

    /**
     * Resizes the current window to a certain width and height.
     *
     * @param int $width
     *   The new width to set on the window.
     * @param null|int $height
     *   The new height to set on the window. By default it will keep the current height.
     */
    public function resizeCurrentWindow($width, $height = null)
    {
        if (empty($height)) {
            $current_size = $this->currentWindow()->size();
            $height = $current_size['height'];
        }

        $this->currentWindow()->size(array('width' => $width, 'height' => $height));
    }

    /**
     * Waits until an element has gone stale.
     *
     * @param \PHPUnit_Extensions_Selenium2TestCase_Element $element
     *   The element we want to wait until becomes stale.
     */
    public function waitUntilElementIsStale(\PHPUnit_Extensions_Selenium2TestCase_Element $element)
    {
        $webdriver = $this;

        $callback = new SerializableClosure(
            function () use ($webdriver, $element) {
                try {
                    // Try accessing the element. If no exception is thrown, it means
                    // that the element is still in the page.
                    $element->displayed();
                } catch (\PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
                    // The element is stale now.
                    return true;
                }
            }
        );
        $this->waitUntil($callback, $this->getTimeout());
    }
}
